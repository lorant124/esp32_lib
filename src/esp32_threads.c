/*
 * esp32_threads.c
 *
 *  Created on: 12. 5. 2020
 *      Author: Lorant
 */
#include "cmsis_os.h"
#include "dma.h"
#include "usart.h"
#include "queue.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "debug_io.h"
#include "esp32.h"
#include "esp32_threads.h"
#include "web_interface.h"

/*
 * Task Name 	: esp_rx_dma_task
 * Description 	: Task watches esp32_rx_dma_queue_id. Queue is waiting
 * for signalization char. Task calculates data lenght and with
 * add_to_queue function saves data and data lenght to esp32_rx_control_QueueHandle
 */
void esp_rx_dma_task(void *argument)
{
	void* d;
    static size_t old_pos;
	size_t pos;
	static char rx_buff_tmp[RX_DMA_CIRCULAR_BUFF_LEN] = {"\0"};
	size_t cnt =0;

  for(;;)
  {
	xQueueReceive(esp32_rx_dma_queue_idHandle, &d, portMAX_DELAY);
	pos = ARRAY_LEN(esp32_rx_dma_buffer) - LL_DMA_GetDataLength(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);

	if (pos != old_pos)
	{
		if (pos > old_pos)
		{

			add_to_queue(&esp32_rx_dma_buffer[old_pos], pos - old_pos);
		}
		else
		{
			strncpy(&rx_buff_tmp[0], &esp32_rx_dma_buffer[old_pos], ARRAY_LEN(esp32_rx_dma_buffer) - old_pos);
			if (pos > 0)
			{
				strncpy(&rx_buff_tmp[ARRAY_LEN(esp32_rx_dma_buffer) - old_pos], &esp32_rx_dma_buffer[0], pos);
				cnt =  (ARRAY_LEN(esp32_rx_dma_buffer) - old_pos) + pos;
				add_to_queue(&rx_buff_tmp, cnt);

			}
			else
			{
				cnt = (ARRAY_LEN(esp32_rx_dma_buffer) - old_pos);
				add_to_queue(&rx_buff_tmp, cnt);
			}
		}
	}
	old_pos = pos;

	if (old_pos == ARRAY_LEN(esp32_rx_dma_buffer))
	{
		old_pos = 0;
	}

    osDelay(1);
  }
}

 /*
 * Task Name 	: esp_ctrl_consumer_task
 * Description 	: Data from esp_rx_dma_task are processed. If
 * data are complete (with valid start and end sequence) task
 * classifies the message type to AT command and URC message.
 * If message is AT command task calls AT parser, if URC calls URC parser.
 * If everything is valid task release blocking semaphore for message synchronization.
 */
void esp_ctrl_consumer_task(void *argument)
{
	static control_msg_t esp_cmd = {{"\0"},0};
	uint8_t msg_flow = 0;
	uint8_t msg_merge_ret = 0;
	control_msg_t rec_msg_tmp = {{"\0"},0};
  for(;;)
  {
	xQueueReceive(esp32_rx_control_QueueHandle, &rec_msg_tmp, portMAX_DELAY);

	  if(msg_flow == 0)
	  {
		  memset(esp_cmd.RxBuff, 0, Q_MSG_RXBUFF_LEN);
		  strncpy(esp_cmd.RxBuff, rec_msg_tmp.RxBuff, rec_msg_tmp.len);
		  esp_cmd.len = rec_msg_tmp.len;
	  }
	 else
	  {
		  if(msg_flow > 3)
		  {
			  memset(esp_cmd.RxBuff, 0, Q_MSG_RXBUFF_LEN);
			  osTimerStop(ESP_MSG_Timeout02Handle);
			  xQueueReset(esp32_rx_control_QueueHandle);
			  osSemaphoreRelease(producer_sync_semHandle);
			  msg_flow = 0;
		  }
		  if(esp_cmd.len+rec_msg_tmp.len >= Q_MSG_RXBUFF_LEN )
		  {
			  memset(esp_cmd.RxBuff, 0, Q_MSG_RXBUFF_LEN);
			  esp_cmd.len = 0;
			  msg_flow = 0;
#ifdef ENABLE_DEBUG_UART_TX
			debug_print("Merged MSG too big\r\n", 20);
#endif
		  }

		  strncpy(&esp_cmd.RxBuff[esp_cmd.len],rec_msg_tmp.RxBuff, rec_msg_tmp.len);
		  esp_cmd.len += rec_msg_tmp.len;
		  msg_flow++;
	  }

	  msg_merge_ret = msg_str_merge(esp_cmd);

	  if(msg_merge_ret == MSG_AT)
	  {
		  msg_flow = 0;
		  AT_parser(esp_cmd);

		  memset(esp_cmd.RxBuff, 0, Q_MSG_RXBUFF_LEN);
		  osTimerStop(ESP_MSG_Timeout02Handle);
		  osDelay(50);
		  osSemaphoreRelease(producer_sync_semHandle);
	  }
	  else if (msg_merge_ret == MSG_URC)
	  {

		memset(esp_cmd.RxBuff, 0, Q_MSG_RXBUFF_LEN);
		msg_flow = 0;
		esp_cmd.len = 0;
		osSemaphoreRelease(producer_sync_semHandle);
	  }
	  else if(msg_merge_ret == MSG_RDY)
	  {
		  esp_init();
	  }
	  else
	  {
		  msg_flow = 1;
	  }

	debug_print(rec_msg_tmp.RxBuff, rec_msg_tmp.len);

    osDelay(1);
  }

}

/*
 * Task Name 	: esp_ctrl_producting_task
 * Description 	: Task is waiting for semaphore release form
 * esp_ctrl_consumer_task (sync). If sempahore is released task
 * is waiting for data in esp32_tx_control_Queue. If data are available,
 * task reads them from Queue and sends to esp32 over uart with DMA.
 */
void esp_ctrl_producting_task(void *argument)
{
	control_msg_t transmit_msg_tmp = {{"\0"},0};

	for(;;)
	{
	  osSemaphoreAcquire(producer_sync_semHandle, portMAX_DELAY);
	  xQueueReceive(esp32_tx_control_QueueHandle, &transmit_msg_tmp, portMAX_DELAY);
	  esp_start_tx_dma_transfer(transmit_msg_tmp.RxBuff,  transmit_msg_tmp.len);
	  osTimerStart(ESP_MSG_Timeout02Handle, 10000);
	  osDelay(1);
	}

}

/*
 * Task Name 	: esp_coordinator_task
 * Description 	: Task monitors the connection status between Wi-Fi and TCP server.
 * If disconnected it tries to reconnect.
 */
void esp_coordinator_task(void *argument)
{
	/*TODO: change to r/w flash memory or eeprom*/
	set_wifi_conn("xiaomi_B927","12345678*");
	set_wifi_tcp_conn("192.168.31.248", 23);
	set_webserver_port(80);
	set_login("root");
	set_pwd("admin");

	while(1)
	{
		if(get_esp_alieve() == ALIEVE) /*Init esp32*/
		{
			osDelay(30000);	/*wait for connection to wifi and connect to server*/
			break;
		}
		osDelay(1000);
	}
  for(;;)
  {
	  if(esp.conn != CONNECTED_W_IP)/* if wifi disconnected  init esp32 again */
	  {
		  esp_init();
		  osDelay(30000);
	  }
	  else if(esp.tcp_conn != TCP_CONNECTED) /*if disconnected from server try connect again*/
	  {
		 at_tcp_connect(TCP_CLIENT_LINK_ID);
	  }
	  else
	  {
		 send_AT("AT+CIPSTATUS\r\n", 20);
	  }

    osDelay(10000);
  }
}

/*
 * Task Name 	: esp_ctrl_producting_task
 * Description 	: Task is waiting for last message delivery (semaphore sync).
 * It checkes user given data in transparent_Queue. If data are available task
 * reads message lenght and generates AT command for sending.
 */
void esp_transparent_data_task(void *argument)
{

  serv_clt_msg_t msg = {{"\0"},0,0};
  for(;;)
  {
	  osSemaphoreAcquire(usr_msg_sync_semHandle, portMAX_DELAY);
	  xQueuePeek(transparent_QueueHandle, &msg,  portMAX_DELAY);
	  at_tcp_fixlen_str_send(msg.link_id, msg.len);
	  osTimerStart(ESP_MSG_Timeout01Handle, 5000);
    osDelay(1);
  }
}

void ESP_MSG_Timeout01_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout01_Callback */
	osSemaphoreRelease(usr_msg_sync_semHandle);

#ifdef ENABLE_DEBUG_UART_TX
	debug_print("ESP_MSG_TIMER01 Timeout\r\n", 25);
#endif
  /* USER CODE END ESP_MSG_Timeout01_Callback */
}


void ESP_MSG_Timeout02_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout02_Callback */
	osSemaphoreRelease(producer_sync_semHandle);

#ifdef ENABLE_DEBUG_UART_TX
	debug_print("ESP_MSG_TIMER02 Timeout\r\n", 25);
#endif
  /* USER CODE END ESP_MSG_Timeout02_Callback */
}

void ESP_MSG_Timeout03_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout03_Callback */

  /* USER CODE END ESP_MSG_Timeout03_Callback */
}
