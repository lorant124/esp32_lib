#include "cmsis_os.h"
#include "dma.h"
#include "usart.h"
#include "esp32.h"
#include "queue.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "web_interface.h"

#ifdef ENABLE_DEBUG_UART_TX
#include "debug_io.h"
#endif

/*global var*/
esp_state_t esp; /*esp connection state*/
wifi_ap_t wifi; /*wifi login data*/
/**/

char esp32_rx_dma_buffer[RX_DMA_CIRCULAR_BUFF_LEN]; /*buffer for DMA*/

/*
 * Task Name 	: set_esp_conn
 * Description 	: Function sets global variable
 * for Wi-Fi connection state and error code.
 */
void set_esp_conn(esp_conn_enum tmp, uint8_t err_code)
{
	taskENTER_CRITICAL();
	esp.conn = tmp;
	esp.conn_err_code = err_code;
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_esp_tcp_conn
 * Description 	: Function sets global variable
 * for TCP server connection state and error code.
 */
void set_esp_tcp_conn(esp_tcp_conn_enum tmp, uint8_t err_code)
{
	taskENTER_CRITICAL();
	esp.tcp_conn = tmp;
	esp.tcp_conn_err_code = err_code;
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: get_esp_conn
 * Description 	: Function returns Wi-Fi connection status
 * from global variable.
 */
esp_conn_enum get_esp_conn(void)
{
	return esp.conn;
}

/*
 * Task Name 	: set_esp_alieve
 * Description 	: Function sets global variable
 * for esp32 living status.
 */
void set_esp_alieve(esp_alieve_enum tmp)
{
	taskENTER_CRITICAL();
	esp.alieve = tmp;
	taskEXIT_CRITICAL();
}


/*
 * Task Name 	: get_esp_alieve
 * Description 	: Function returns living status
 * from global variable.
 */
esp_alieve_enum get_esp_alieve(void)
{
	return esp.alieve;
}

/*
 * Task Name 	: set_wifi_conn
 * Description 	: Function sets global variables
 * for storage Wi-Fi SSID and password.
 */
void set_wifi_conn(const void* ssid, const void* pwd)
{
	const char* s = ssid;
	const char* p = pwd;
	taskENTER_CRITICAL();
	strcpy(wifi.ssid,s);
	strcpy(wifi.pwd,p);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_wifi_tcp_conn
 * Description 	: Function sets global variables
 * for storage TCP IP address and port.
 */
void set_wifi_tcp_conn(const void* ip, const uint32_t port)
{
	const char* i = ip;
	taskENTER_CRITICAL();
	strcpy(wifi.tcp_ip,i);
	wifi.tcp_port = port;
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_webserver_port
 * Description 	: Function sets global variables
 * for storage webserver port.
 */
void set_webserver_port(uint32_t port)
{
	taskENTER_CRITICAL();
	wifi.server_port = port;
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_webserver_ip
 * Description 	: Function sets global variables
 * for storage webserver IP address.
 */
void set_webserver_ip(const void* ip)
{
	const char* i = ip;
	taskENTER_CRITICAL();
	strcpy(wifi.server_ip,i);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_gateway_ip
 * Description 	: Function sets global variables
 * for storage Wi-Fi gateway IP address.
 */
void set_gateway_ip(const void* ip)
{
	const char* i = ip;
	taskENTER_CRITICAL();
	strcpy(wifi.gateway_ip,i);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_netmask
 * Description 	: Function sets global variables
 * for storage Wi-Fi network mask address.
 */
void set_netmask(const void* netmask)
{
	const char* i = netmask;
	taskENTER_CRITICAL();
	strcpy(wifi.netmask,i);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_login
 * Description 	: Function sets global variables
 * for storage webserver login.
 */
void set_login(const void* login)
{
	const char* i = login;
	taskENTER_CRITICAL();
	strcpy(wifi.login,i);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: set_pwd
 * Description 	: Function sets global variables
 * for storage webserver password.
 */
void set_pwd(const void* pwd)
{
	const char* i = pwd;
	taskENTER_CRITICAL();
	strcpy(wifi.login_pwd,i);
	taskEXIT_CRITICAL();
}

/*
 * Task Name 	: esp_sw_rst
 * Description 	: Function resets all queues, semaphores and
 * sends software reset command to esp32.
 */
void esp_sw_rst(void)
{
	 xQueueReset(esp32_rx_control_QueueHandle);
	 xQueueReset(esp32_tx_control_QueueHandle);
	 xQueueReset(transparent_QueueHandle);
	 osSemaphoreRelease(producer_sync_semHandle);
	 osSemaphoreRelease(usr_msg_sync_semHandle);
	 set_esp_alieve(NOTALIEVE);
	 send_AT("AT+RST\r\n", 8);
	 osDelay(5000);
	 esp_init();
}

/*
 * Task Name 	: esp32_uart_setup
 * Description 	: Function setups uart with DMA and idle
 * line detection for esp32 communication.
 */
void esp32_uart_setup(void)
{
	/*USART2 RX*/
	LL_DMA_SetPeriphAddress(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, (uint32_t)&ESP32_UART->RDR);
	LL_DMA_SetMemoryAddress(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, (uint32_t)esp32_rx_dma_buffer);
	LL_DMA_SetDataLength(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, ARRAY_LEN(esp32_rx_dma_buffer));
	//LL_DMA_EnableIT_HT(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);
	//LL_DMA_EnableIT_TC(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);
	LL_DMA_EnableStream(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);

	LL_USART_EnableDMAReq_RX(ESP32_UART);
	LL_USART_EnableIT_IDLE(ESP32_UART);

	/*UART2 TX*/
	LL_DMA_EnableIT_TC(ESP32_TX_DMA, ESP32_TX_DMA_STREAM);
	LL_DMA_SetPeriphAddress(ESP32_TX_DMA, ESP32_TX_DMA_STREAM, (uint32_t)&ESP32_UART->TDR);
	LL_USART_EnableDMAReq_TX(ESP32_UART);

}

/*
 * Task Name 	: esp32_UART_Handler
 * Description 	: Handler handles uart idle line interrupts. If interruption
 *  occures function saves signalization char to esp32_rx_dma_queue_id.
 */
void esp32_UART_Handler(void) {
    void* d = (void *)1;
    /* Check for IDLE line interrupt */
    if (LL_USART_IsEnabledIT_IDLE(ESP32_UART) && LL_USART_IsActiveFlag_IDLE(ESP32_UART)) {
        LL_USART_ClearFlag_IDLE(ESP32_UART);        /* Clear IDLE line flag */
        osMessageQueuePut(esp32_rx_dma_queue_idHandle, &d, 0, 0);
    }
}

/*
 * Task Name 	: esp_init
 * Description 	: Function generates AT commands for esp32 initialization.
 */
void esp_init(void)
{
	send_AT("AT+CWMODE=1\r\n", 13);
	at_wifi_connect();
	send_AT("AT+CIPMODE=0\r\n\0", 15);			/*transmission mode*/
	send_AT("AT+CIPMUX=1\r\n", 13);				/*multiple conn mode*/
	at_tcp_connect(TCP_CLIENT_LINK_ID);
	send_AT("AT+CIPSERVERMAXCONN=3\r\n", 23); 	/*max connection*/
	at_webserver_start();
	send_AT("AT+CIPSTA?\r\n", 12);				/*Get Status*/
}

/*
 * Task Name 	: at_wifi_connect
 * Description 	: Function generates AT command for Wi-Fi conenction
 * with stored SSID and password.
 */
void at_wifi_connect(void)
{
	char cmd_buff[50] = {0};
	snprintf(cmd_buff, 50, "AT+CWJAP=\"%s\",\"%s\"\r\n", wifi.ssid, wifi.pwd);
	send_AT(cmd_buff,strlen(cmd_buff));
}

/*
 * Task Name 	: at_webserver_start
 * Description 	: Function generates AT command for webserver starting
 * with stored webserver port.
 */
void at_webserver_start(void)
{
	char cmd_buff[25] = {0};
	snprintf(cmd_buff, 25, "AT+CIPSERVER=%d,%lu\r\n", WEB_SERVER_LINK_ID, wifi.server_port);
	send_AT(cmd_buff, strlen(cmd_buff));
	send_AT("AT+CIPSTO=25\r\n", 14);
}

/*
 * Task Name 	: at_tcp_connect
 * Description 	: Function generates AT command for connection to TCP server
 * with stored TCP IP address and TCP port.
 */
void at_tcp_connect(uint8_t link_id)
{
	char cmd_buff[100] = {0};
	snprintf(cmd_buff, 100, "AT+CIPSTART=%d,\"TCP\",\"%s\",%ld\r\n",link_id ,wifi.tcp_ip, wifi.tcp_port);
	send_AT(cmd_buff,strlen(cmd_buff));
}

/*
 * Task Name 	: at_tcp_fixlen_str_send
 * Description 	: Function generates AT command for message sending with fixed lenght.
 */
void at_tcp_fixlen_str_send(uint8_t link_id, uint8_t len)
{
	char cmd_buff[25] = {0};
	taskENTER_CRITICAL();
	snprintf(cmd_buff, 25, "AT+CIPSEND=%d,%d\r\n", link_id, len);
	taskEXIT_CRITICAL();
	send_AT(cmd_buff,strlen(cmd_buff));

}

/*
 * Task Name 	: send_AT
 * Description 	: Function generates control_msg_t and adds AT command to esp32_tx_control_Queue.
 */
void send_AT(const void* data, uint8_t len)
{
	control_msg_t tx_ctrl_msg = {{'\0'}, 0};
	const char* d = data;
	taskENTER_CRITICAL();
	strncpy(tx_ctrl_msg.RxBuff, d, len);
	tx_ctrl_msg.len = len;
	taskEXIT_CRITICAL();
	xQueueSend(esp32_tx_control_QueueHandle, &tx_ctrl_msg, 100);
}

/*
 * Task Name 	: esp_start_tx_dma_transfer
 * Description 	: Function sets up DMA to send fixed lenght data and enable stream.
 */
uint8_t esp_start_tx_dma_transfer(const void* data, uint8_t len) {

    uint32_t old_primask;
    uint8_t started = 0;
    const char* d = data;
    /* Check if DMA is active */
    /* Must be set to 0 */
    old_primask = __get_PRIMASK();
    __disable_irq();

	if (len > 0) {
		/* Disable channel if enabled */
		LL_DMA_DisableStream(ESP32_TX_DMA, ESP32_TX_DMA_STREAM);
		/* Clear all flags */
		/*LL_DMA_ClearFlag_TC6(ESP32_TX_DMA);
		LL_DMA_ClearFlag_HT6(ESP32_TX_DMA);
		LL_DMA_ClearFlag_TE6(ESP32_TX_DMA);
		LL_DMA_ClearFlag_DME6(ESP32_TX_DMA);
		LL_DMA_ClearFlag_FE6(ESP32_TX_DMA);*/
		/* Start DMA transfer*/
		LL_DMA_SetDataLength(ESP32_TX_DMA, ESP32_TX_DMA_STREAM, len);
		LL_DMA_SetMemoryAddress(ESP32_TX_DMA, ESP32_TX_DMA_STREAM, (uint32_t)d);

		/* Start new transfer */
		LL_DMA_EnableStream(ESP32_TX_DMA, ESP32_TX_DMA_STREAM);
		started = 1;
	}

    __set_PRIMASK(old_primask);

    return started;
}

/*
 * Task Name 	: add_to_queue
 * Description 	: Function saves received data from esp_rx_dma_task to esp32_rx_control_Queue.
 * If data are IPD it checkes if data does not contain AT command.
 * If it contains data are saves separately.
 */
void add_to_queue(const void* data, size_t len)
{
	const char* d =  data;
	char* ptr = NULL;
	char* ptr2 = NULL;
	char cnt[5] = {'\0'};
	control_msg_t msg ={{"\0"},0};
	control_msg_t msg_splited ={{"\0"},0};
	uint16_t pos_len = 0;
	uint16_t ipd_len = 0;
	uint8_t i = 0;

	ptr = strstr(d, IPD);
	pos_len = ptr - d;
	if(ptr != NULL && pos_len < len)
	{
		ptr += 7;
		while(*ptr != ':' || i < 4)
		{
			cnt[i] = *ptr;
			i++;
			ptr++;
		}
		ipd_len = atoi(cnt);
		ipd_len += 14;
		if(ipd_len != len)
		{
			ptr = strstr(d, IPD);
			pos_len = ptr - d;
			if(ptr != NULL && pos_len < len)
			{
				ptr = strstr(d, AT);
				pos_len = ptr - d;
				if(ptr != NULL && pos_len < len)
				{
					ptr2 = strstr(ptr, AT_OK);
					pos_len = ptr2 - d;
					if(ptr2 != NULL && pos_len < len)
					{
						ptr2 +=  4;
						strncpy(&msg_splited.RxBuff[0], ptr, ptr2 - ptr);
						msg_splited.len = ptr2 - ptr;
						xQueueSend(esp32_rx_control_QueueHandle, &msg_splited, 0);
					}
					else
					{
						ptr2 = strstr(ptr, AT_ERROR);
						pos_len = ptr2 - d;
						if(ptr2 != NULL && pos_len < len)
						{
							ptr2 +=  7;
							strncpy(&msg_splited.RxBuff[0], ptr, ptr2 - ptr);
							msg_splited.len = ptr2 - ptr;
							xQueueSend(esp32_rx_control_QueueHandle, &msg_splited, 0);

						}
					}
				}
			}
		}

	}

	if(len > Q_MSG_RXBUFF_LEN)
	{
		debug_print("Msg is too big\n",15);
		ptr = strstr(d, IPD);
		pos_len = ptr - d;

		if((ptr != NULL) && pos_len < len)
		{
			ptr2 = strstr(ptr,"GET /");
			pos_len = ptr2 - d;
			if(ptr2 != NULL  && pos_len < len)
			{
				ptr2 = strstr(ptr,"\r\n");
				pos_len = ptr2 - d;
				if(ptr2 != NULL && pos_len < len)
				{
					strncpy(msg.RxBuff, ptr, ptr2 - ptr);
					msg.len = ptr2 - ptr;
				}

				ptr2 = strstr(ptr,"\r\n\r\n");
				pos_len = ptr2 - d;
				if(ptr2 != NULL && pos_len < len)
				{
					ptr2 += 4;
					pos_len = ptr2 - d;
					if(((len - pos_len) + msg.len) < Q_MSG_RXBUFF_LEN)
					{
						strncpy(&msg.RxBuff[msg.len], ptr2, len - pos_len);
						msg.len += len - pos_len;
					}
					else
					{
						strncpy(&msg.RxBuff[msg.len], ptr2, Q_MSG_RXBUFF_LEN-msg.len);
						msg.len = Q_MSG_RXBUFF_LEN;
					}
				}
				xQueueSend(esp32_rx_control_QueueHandle, &msg, 0);
				return;
			}

			ptr2 = strstr(ptr,"POST /");
			pos_len = ptr2 - d;

			if(ptr2 != NULL && pos_len < len)
			{
				ptr2 = strstr(ptr,".html");
				pos_len = ptr2 - d;
				if(ptr2 != NULL && pos_len < len)
				{
					strncpy(msg.RxBuff, ptr, ptr2 - ptr);
					msg.len = ptr2 - ptr;
				}

				ptr2 = strstr(ptr,"\r\n\r\n");
				pos_len = ptr2 - d;
				if(ptr2 != NULL && pos_len < len)
				{
					ptr2 += 4;
					pos_len = ptr2 - d;
					if(((len - pos_len) + msg.len) < Q_MSG_RXBUFF_LEN)
					{
						strncpy(&msg.RxBuff[msg.len], ptr2, len - pos_len);
						msg.len += len - pos_len;
					}
					else
					{
						strncpy(&msg.RxBuff[msg.len], ptr2, Q_MSG_RXBUFF_LEN-msg.len);
						msg.len = Q_MSG_RXBUFF_LEN;
					}
				}
				xQueueSend(esp32_rx_control_QueueHandle, &msg, 0);
				return;
			}

		}

		strncpy(msg.RxBuff, &d[0], Q_MSG_RXBUFF_LEN);
		msg.len = Q_MSG_RXBUFF_LEN;
		xQueueSend(esp32_rx_control_QueueHandle, &msg, 0);
		return;
	}
	strncpy(msg.RxBuff, &d[0], len);
	msg.len = len;
	xQueueSend(esp32_rx_control_QueueHandle, &msg, 0);
}

/*
 * Task Name 	: msg_str_merge
 * Description 	: Function checkes if data are AT command response.
 * If they are not function checkes if data are URC msg.
 */
urc_ans_enum msg_str_merge(control_msg_t msg)
{
	if(strncmp(msg.RxBuff, AT, 3) == 0)
	{
		if(strstr(msg.RxBuff, AT_OK) != NULL || strstr(msg.RxBuff, AT_ERROR) != NULL)
		{
			return MSG_AT;
		}
		else
		{
			return MSG_UNKNOWN;
		}
	}
	else
	{
		return parse_urc(msg);
	}

	return MSG_UNKNOWN;
}

/*
 * Task Name 	: AT_parser
 * Description 	: Function checkes type of AT command response.
 * If the type can be found, the appropriate function is called.
 */
uint8_t AT_parser(control_msg_t msg)
{
	if(strstr(msg.RxBuff, AT_CWJAP) != NULL)
	 {
		 return parse_cwjap(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSTART) != NULL)
	 {
		 return parse_cipstart(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPMODE) != NULL)
	 {
		 return parse_cipmode(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSTATUS) != NULL)
	 {
		 return parse_cipstatus(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSEND) != NULL)
	 {
		 return parse_cipsend(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CWMODE) != NULL)
	 {
		 return parse_cwmode(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPMUX) != NULL)
	 {
		 return parse_cipmux(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSERVER) != NULL)
	 {
		 return parse_cipmux(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSTO) != NULL)
	 {
		 return parse_cipsto(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSTA) != NULL)
	 {
		 return parse_cipsta(msg);
	 }
	else if(strstr(msg.RxBuff, AT_RST) != NULL)
	 {
		 return parse_rst(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPSERVERMAXCONN) != NULL)
	 {
		 return parse_cipservermaxconn(msg);
	 }
	else if(strstr(msg.RxBuff, AT_CIPCLOSE) != NULL)
	 {
		 return parse_cipclose(msg);
	 }

	else
	{
#ifdef ENABLE_DEBUG_UART_TX
			debug_print("UNKNOWN AT MSG:\r\n", 17);
			debug_print(msg.RxBuff, msg.len);
#endif
	}

	return 0;
}

/*
 * Task Name 	: parse_cwjap
 * Description 	: Function processes Wi-Fi connect setup AT command response.
 */
uint8_t parse_cwjap(const control_msg_t msg)
{
	char* ptr = 0;

	if(strstr((char*)msg.RxBuff, "CONNECTED") != NULL)
	{
		set_esp_conn(CONNECTED, 0);
	}

	if(strstr((char*)msg.RxBuff, "GOT IP") != NULL)
	{
		set_esp_conn(CONNECTED_W_IP, 0);
	}

	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	else if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{

		ptr = strstr((char*)msg.RxBuff, "+CWJAP:");
		if( ptr != NULL)
		{
			set_esp_conn(CONNECTION_ERROR,atoi(ptr+8));
		}
		else
		{
			set_esp_conn(CONNECTION_ERROR,0);
		}
		return 1;
	}
	else
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CWJAP MSG FORMAT ERROR\r\n",24);
#endif
	}

	return 0;
}

/*
 * Task Name 	: parse_cipstart
 * Description 	: Function processes TCP server connect setup AT command response.
 */
uint8_t parse_cipstart(const control_msg_t msg)
{
	char* ptr = 0;

	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		set_esp_tcp_conn(TCP_CONNECTED, 0);

		return 1;
	}

	else if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{

		ptr = strstr((char*)msg.RxBuff, "+CIPSTART:");
		if( ptr != NULL)
		{
			set_esp_tcp_conn(TCP_ERROR,atoi(ptr+12));
		}
		else
		{
			set_esp_tcp_conn(TCP_ERROR,0);
		}
		return 1;
	}
	else
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSTART MSG FORMAT ERROR\r\n",27);
#endif
	}
	return 0;
}

/*
 * Task Name 	: parse_cipmode
 * Description 	: Function processes transmit mode setup AT command response.
 */
uint8_t parse_cipmode(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPMODE ERROR\r\n", 15);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipserver
 * Description 	: Function processes webserver start setup AT command response.
 */
uint8_t parse_cipserver(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSERVER ERROR\r\n", 17);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipclose
 * Description 	: Function processes connection close AT command response.
 */
uint8_t parse_cipclose(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPCLOSE ERROR\r\n", 16);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_rst
 * Description 	: Function processes reset AT command response.
 */
uint8_t parse_rst(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{

		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipmux
 * Description 	: Function processes multiple connection setup AT command response.
 */
uint8_t parse_cipmux(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPMUX ERROR\r\n", 14);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipsto
 * Description 	: Function processes client connection timeout setup AT command response.
 */
uint8_t parse_cipsto(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSTO ERROR\r\n", 14);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cwmode
 * Description 	: Function processes Wi-Fi mode setup AT command response.
 */
uint8_t parse_cwmode(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CWMODE ERROR\r\n", 14);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipservermaxconn
 * Description 	: Function processes max client connection setup AT command response.
 */
uint8_t parse_cipservermaxconn(const control_msg_t msg)
{
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSERVERMAXCONN ERROR\r\n", 24);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipsta
 * Description 	: Function processes network status AT command response.
 */
uint8_t parse_cipsta(const control_msg_t msg)
{
	char* ptr1 = NULL;
	char* ptr2 = NULL;
	char tmp[25] = {'\0'};
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		ptr1 = strstr(msg.RxBuff, "+CIPSTA:ip:\"");
		if(ptr1 != NULL)
		{
			ptr1 += 12;
			ptr2 = strstr(msg.RxBuff, "+CIPSTA:gateway:\"");
			ptr2 = ptr2 -3;
			strncpy(&tmp[0], ptr1, ptr2-ptr1);
			tmp[ptr2-ptr1] = '\0';
			set_webserver_ip(tmp);
		}

		ptr1 = strstr(msg.RxBuff, "+CIPSTA:gateway:\"");
		if(ptr1 != NULL)
		{
			ptr1 += 17;
			ptr2 = strstr(msg.RxBuff, "+CIPSTA:netmask:\"");
			ptr2 = ptr2 -3;
			strncpy(&tmp[0], ptr1, ptr2-ptr1);
			tmp[ptr2-ptr1] = '\0';
			set_gateway_ip(tmp);
		}

		ptr1 = strstr(msg.RxBuff, "+CIPSTA:netmask:\"");
		if(ptr1)
		{
			ptr1 += 17;
			ptr2 = strstr(msg.RxBuff, "\r\nOK\r\n");
			ptr2 = ptr2 -3;
			strncpy(&tmp[0], ptr1, ptr2-ptr1);
			tmp[ptr2-ptr1] = '\0';
			set_netmask(tmp);
		}
		return 1;
	}
	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSTA ERROR\r\n", 14);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipsend
 * Description 	: Function processes data send AT command response.
 * If response containes OK, message is given from transparent_Queue
 * to esp32_tx_control_Queue and producing task sends user message to esp32.
 */
uint8_t parse_cipsend(const control_msg_t msg)
{
	serv_clt_msg_t tx_msg = {{"\0"}, 0};
	if(strstr((char*)msg.RxBuff, AT_OK) != NULL)
	{
		osTimerStop(ESP_MSG_Timeout02Handle);
		xQueueReceive(transparent_QueueHandle, &tx_msg,  0);
		xQueueSendToFront(esp32_tx_control_QueueHandle, &tx_msg,  0);
		return 1;
	}

	if(strstr((char*)msg.RxBuff, AT_ERROR) != NULL)
	{
		osTimerStop(ESP_MSG_Timeout02Handle);
#ifdef MSG_RPT_OFF
		xQueueReceive(transparent_QueueHandle, &tx_msg,  0);
#endif
		osSemaphoreRelease(usr_msg_sync_semHandle);

#ifdef ENABLE_DEBUG_UART_TX
		debug_print("CIPSEND MSG ERROR\r\n", 19);
#endif
		return 1;
	}
	return 0;
}

/*
 * Task Name 	: parse_cipstatus
 * Description 	: Function processes connection status AT command response.
 */
uint8_t parse_cipstatus(const control_msg_t msg)
{
	/*TODO:multiconnection status parse*/
	char* ptr = NULL;
	ptr = strstr((char*)msg.RxBuff, "\r\nSTATUS:");

	if( ptr != NULL)
	{
		switch(atoi(ptr+9))
		{
		case 2:
			set_esp_tcp_conn(TCP_ERROR,0);
			return 1;
			break;
		case 3:
			return 1;
			break;
		case 4:
			set_esp_tcp_conn(CLOSED,0);
			return 1;
			break;
		case 5:
			set_esp_conn(CONNECTION_ERROR,0);
			set_esp_tcp_conn(TCP_ERROR,0);
			return 1;
			break;
		default:
			set_esp_conn(CONNECTION_ERROR,0);
			set_esp_tcp_conn(TCP_ERROR,0);
			return 1;
			break;
		}
	}
	return 0;
}

/*
 * Task Name 	: parse_urc
 * Description 	: Function checkes if message is a URC message
 * if yes the message is processed.
 */
urc_ans_enum parse_urc(const control_msg_t msg)
{
	char* ptr = 0;
	uint8_t client_link_id = 0;
	uint8_t ack_cnt = 0;

	if(strncmp((char*)msg.RxBuff, RDY, 9) == 0)
	{
		 set_esp_alieve(ALIEVE);
		 return MSG_RDY;
	}

	ptr =strstr(msg.RxBuff, AT_TCP_CLOSED);
	if(ptr != NULL)
	{
		ack_cnt ++;
		if(*(ptr-1) == '0' && esp.tcp_conn == TCP_CONNECTED)
		{
			 osTimerStop(ESP_MSG_Timeout01Handle);
			 osTimerStop(ESP_MSG_Timeout02Handle);

			set_esp_tcp_conn(CLOSED, 0);


		}
		else
		{
			//return MSG_CLIENT_DISCONNECT;
		}
	}

	if(strstr(msg.RxBuff, AT_TCP_CONNECT) != NULL)
	{
		ack_cnt ++;
	}

	if (strstr((char*)msg.RxBuff, RECV_SEND_OK) != NULL || strstr((char*)msg.RxBuff, RECV_SEND_ERROR) != NULL)
	{
		ack_cnt ++;
		osTimerStop(ESP_MSG_Timeout01Handle);
		osSemaphoreRelease(usr_msg_sync_semHandle);
		/*TODO: SEND FAIL*/
	}

	ptr = strstr((char*)msg.RxBuff, "+IPD");
	if(ptr != NULL)
	{
		ack_cnt ++;
		client_link_id = atoi(ptr+5);

		if((client_link_id > 0) && esp.tcp_conn == TCP_CONNECTED)
			client_communication(client_link_id, msg);
		else if(esp.tcp_conn != TCP_CONNECTED)
			client_communication(client_link_id, msg);
	}

	if(ack_cnt)
		return MSG_URC;
	else
		return MSG_UNKNOWN;
}
