/*
 * web_interface.c
 *
 *  Created on: 17. 5. 2020
 *      Author: Lorant
 */
#include "cmsis_os.h"
#include "dma.h"
#include "usart.h"
#include "esp32.h"
#include "queue.h"

#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "web_interface.h"

extern wifi_ap_t wifi; /*wifi login data*/

const char* login_page  = {
"<!DOCTYPE html>\
<html lang=\"en\"> \
<head> \
<meta charset=\"UTF-8\"> \
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
<link rel=\"icon\" href=\"data:,\"> \
<title>Login</title> \
<style> \
html { \
height: 100%; \
} \
body { \
height: 100%; \
margin: 0; \
font-family: Arial, Helvetica, sans-serif; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: #3a3a3a; \
} \
#main-holder { \
width: 50%; \
height: 70%; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: white; \
border-radius: 7px; \
box-shadow: 0px 0px 5px 2px black; \
} \
#error-msg-second-line { \
display: block; \
} \
#login-form { \
align-self: flex-start; \
display: grid; \
justify-items: center; \
align-items: center; \
} \
.login-form-field::placeholder { \
color: #3a3a3a; \
} \
.login-form-field { \
border: none; \
border-bottom: 1px solid #3a3a3a; \
margin-bottom: 10px; \
border-radius: 3px; \
outline: none; \
padding: 0px 0px 5px 5px; \
} \
#login-form-submit { \
width: 100%; \
padding: 7px; \
border: none; \
border-radius: 5px; \
color: white; \
font-weight: bold; \
background-color: #3a3a3a; \
cursor: pointer; \
outline: none; \
}  \
</style> \
</head> \
<body> \
<main id=\"main-holder\"> \
<h1 id=\"login-header\">Login</h1> \
<form id=\"login-form\" method=\"post\" action=\"/home_page.html\"> \
<input type=\"text\" name=\"username\" id=\"username-field\" class=\"login-form-field\" placeholder=\"Username\"> \
<input type=\"password\" name=\"password\" id=\"password-field\" class=\"login-form-field\" placeholder=\"Password\"> \
<input type=\"submit\" value=\"Login\" id=\"login-form-submit\"> \
</form> \
</main> \
</body> \
</html>"
};

const char * edit_page  = {
"<!DOCTYPE html> \
<html lang=\"en\"> \
<head> \
<meta charset=\"UTF-8\"> \
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
<link rel=\"icon\" href=\"data:,\"> \
<title>Settings</title> \
<style> \
html { \
height: 100%; \
} \
body { \
height: 70%; \
margin: 0; \
font-family: Arial, Helvetica, sans-serif; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: #3a3a3a; \
} \
#main-holder { \
width: 50%; \
height: 70%; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: white; \
border-radius: 7px; \
box-shadow: 0px 0px 5px 2px black; \
} \
#settings-form { \
line-height: 30%; \
align-self: flex-start; \
display: left; \
justify-items: center; \
align-items: center; \
} \
#button-submit { \
float: left \
width: 50%; \
padding: 5px; \
border: none; \
border-radius: 5px; \
color: white; \
font-weight: bold; \
background-color: #3a3a3a; \
cursor: pointer; \
outline: none; \
} \
</style> \
</head> \
<body> \
<main id=\"main-holder\"> \
<h1 id=\"login-header\">Settings</h1> \
<form id=\"login-form\" method=\"post\" action=\"/saved_page.html\"> \
<p>TCP Server IP address................. <input type=\"text\" name=\"TCPIP\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"192.168.1.1\"></p>\
<p>TCP Server port............................ <input type=\"text\" name=\"TCPPORT\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"23\"></p>\
<p>Netmask........................................ <input type=\"text\" name=\"MASK\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"255.255.255.0\"></p>\
<p>Gateway........................................ <input type=\"text\" name=\"GATEWAY\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"192.168.1.1\"></p>\
<p>SSID.............................................. <input type=\"text\" name=\"SSID\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"WIFI SSID\"></p>\
<p>Wi-Fi password............................. <input type=\"text\" name=\"WIFIPWD\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"PWD1234\"></p>\
<p>Webserver IP................................ <input type=\"text\" name=\"WEBIP\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"192.168.1.2\"></p>\
<p>Webserver Port............................. <input type=\"text\" name=\"WEBPORT\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"80\"></p>\
<p>Login.............................................. <input type=\"text\" name=\"WEBLOGIN\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"root\"></p>\
<p>Password...................................... <input type=\"text\" name=\"WEBPWD\" id=\"username-field\" class=\"settings-form-field\" placeholder=\"admin\"></p>\
<input type=\"submit\" value=\"Save\" id=\"button-submit\">\
</form> \
</main> \
</body> \
</html>"
};

const char home_page[]  = {
"<!DOCTYPE html> \
<html lang=\"en\"> \
<head> \
<meta charset=\"UTF-8\"> \
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
<link rel=\"icon\" href=\"data:,\"> \
<title>Settings</title> \
<style> \
html { \
height: 100%; \
} \
body { \
height: 70%; \
margin: 0; \
font-family: Arial, Helvetica, sans-serif; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: #3a3a3a; \
} \
#main-holder { \
width: 50%; \
height: 70%; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: white; \
border-radius: 7px; \
box-shadow: 0px 0px 5px 2px black; \
} \
#settings-form { \
line-height: 30%; \
align-self: flex-start; \
display: left; \
justify-items: center; \
align-items: center; \
} \
#button-submit { \
float: left \
width: 50%; \
padding: 5px; \
border: none; \
border-radius: 5px; \
color: white; \
font-weight: bold; \
background-color: #3a3a3a; \
cursor: pointer; \
outline: none; \
} \
</style> \
</head> \
<body> \
<main id=\"main-holder\"> \
<h1 id=\"login-header\">Settings</h1> \
<form id=\"settings-form\" method=\"post\" action=\"/edit_page.html\"> "
};

const char home_page_end[]  = {
"<input type=\"submit\" value=\"Edit settings\" id=\"button-submit\"> \
</form> \
</main> \
</body> \
</html>"
};

const char* login_error_page  = {
"<!DOCTYPE html>\
<html lang=\"en\"> \
<head> \
<meta charset=\"UTF-8\"> \
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
<link rel=\"icon\" href=\"data:,\"> \
<title>Login</title> \
<style> \
html { \
height: 100%; \
} \
body { \
height: 100%; \
margin: 0; \
font-family: Arial, Helvetica, sans-serif; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: #3a3a3a; \
} \
#main-holder { \
width: 50%; \
height: 70%; \
display: grid; \
justify-items: center; \
align-items: center; \
background-color: white; \
border-radius: 7px; \
box-shadow: 0px 0px 5px 2px black; \
} \
#error-msg-second-line { \
display: block; \
} \
#login-form { \
align-self: flex-start; \
display: grid; \
justify-items: center; \
align-items: center; \
} \
.login-form-field::placeholder { \
color: #3a3a3a; \
} \
.login-form-field { \
border: none; \
border-bottom: 1px solid #3a3a3a; \
margin-bottom: 10px; \
border-radius: 3px; \
outline: none; \
padding: 0px 0px 5px 5px; \
} \
#login-form-submit { \
width: 100%; \
padding: 7px; \
border: none; \
border-radius: 5px; \
color: white; \
font-weight: bold; \
background-color: #3a3a3a; \
cursor: pointer; \
outline: none; \
}  \
#login-error-msg-holder { \
width: 100%; \
height: 100%; \
display: grid; \
justify-items: center; \
align-items: center; \
} \
#login-error-msg { \
width: 23%; \
text-align: center; \
margin: 0; \
padding: 5px; \
font-size: 12px; \
font-weight: bold; \
color: #8a0000; \
border: 1px solid #8a0000; \
background-color: #e58f8f; \
opacity: 1; \
} \
</style> \
</head> \
<body> \
<main id=\"main-holder\"> \
<h1 id=\"login-header\">Login</h1> \
<div id=\"login-error-msg-holder\"> \
<p id=\"login-error-msg\">Invalid username <span id=\"error-msg-second-line\">and/or password</span></p> \
</div> \
<form id=\"login-form\" method=\"post\" action=\"/home_page.html\"> \
<input type=\"text\" name=\"username\" id=\"username-field\" class=\"login-form-field\" placeholder=\"Username\"> \
<input type=\"password\" name=\"password\" id=\"password-field\" class=\"login-form-field\" placeholder=\"Password\"> \
<input type=\"submit\" value=\"Login\" id=\"login-form-submit\"> \
</form> \
</main> \
</body> \
</html>"
};

/*
 * Task Name 	: send_html
 * Description 	: Function gets html string and active link ID. Function splits html
 * string to 200 char packs and adds them to transparent_Queue.
 */
void send_html(uint8_t link_id, const char* html)
{
	uint16_t html_len = strlen(html);
	uint16_t i = 0;
	serv_clt_msg_t tx_msg = {{'\0'}, 0, 0};
	while( (i+200) < html_len)
	{
		strncpy(tx_msg.message, &html[i], 200);
		tx_msg.len = 200;
		tx_msg.link_id = link_id;
		xQueueSend(transparent_QueueHandle, &tx_msg, 0);
		i = i+200;
	}
	if(i < html_len)
	{
		strncpy(tx_msg.message, &html[i], html_len - i);
		tx_msg.len = html_len - i;
		tx_msg.link_id = link_id;
		xQueueSend(transparent_QueueHandle, &tx_msg, 0);
	}
}

/*
 * Task Name 	: client_communication
 * Description 	: Function processes received data from web page.
 */
void client_communication(uint8_t link_id, const control_msg_t msg) /*GET*/
{
	char* login_ptr1 = NULL;
	char* login_ptr2 = NULL;
	char  client_login[LOGIN_MAXLEN] = {'\0'};
	char  client_login_pwd[LOGIN_PWD_MAXLEN] = {'\0'};
	if(strstr((char*)msg.RxBuff, "GET /") != NULL)
	{
		send_html(link_id, login_page);
	}
	else if(strstr((char*)msg.RxBuff, "POST /home_page" ) != NULL )
	{
		login_ptr1 = strstr((char*)msg.RxBuff, "username=");
		if(login_ptr1 != NULL)
		{
			login_ptr1 += 9;
			login_ptr2 = strstr((char*)msg.RxBuff, "&password=");
			if((login_ptr2-login_ptr1) < LOGIN_MAXLEN-1)
			{
				strncpy(client_login, login_ptr1, login_ptr2-login_ptr1);
				if(strncmp(client_login, wifi.login, strlen(wifi.login)+1) == 0)
				{
					login_ptr2 += 10;
					login_ptr1 = strstr(login_ptr2, "\r\n");
					if((login_ptr1-login_ptr2) < LOGIN_PWD_MAXLEN-1)
					{
						strncpy(client_login_pwd, login_ptr2, login_ptr1 - login_ptr2);
						if(strncmp(client_login_pwd, wifi.login_pwd, strlen(wifi.login_pwd)+1) == 0)
						{
							send_html(link_id, home_page);
							send_html(link_id, create_home_setting_str());
							send_html(link_id, home_page_end);
							return;
						}
					}
				}
			}
		}
		send_html(link_id, login_error_page);
		return;
	}
	else if(strstr((char*)msg.RxBuff, "POST /edit_page") != NULL)
	{
		send_html(link_id, edit_page);
	}
	else if(strstr((char*)msg.RxBuff, "POST /saved_page") != NULL)
	{
		edit_conn_params(msg);
		send_html(link_id, home_page);
		send_html(link_id, create_home_setting_str());
		send_html(link_id, home_page_end);
	}
}

/*
 * Task Name 	: edit_conn_params
 * Description 	: Function processes user settings from web page.
 */
void edit_conn_params(const control_msg_t msg)
{
	char* ptr1 = NULL;
	char* ptr2 = NULL;
	char tmp_buff[20] = {'0'};

	ptr1 = strstr((char*)msg.RxBuff, "TCPIP=");
	ptr2 = strstr((char*)msg.RxBuff, "&TCPPORT=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 6;
		if(ptr2-ptr1 >=7)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_wifi_tcp_conn(tmp_buff, wifi.tcp_port);
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "TCPPORT=");
	ptr2 = strstr((char*)msg.RxBuff, "&MASK=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 8;
		if(ptr2-ptr1 > 0)
		{
			if( ptr2-ptr1 >= 1)
			{
				strncpy(tmp_buff, ptr1, ptr2-ptr1);
				tmp_buff[ptr2-ptr1] = '\0';
				set_wifi_tcp_conn(wifi.tcp_ip, atoi(tmp_buff));
			}
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "MASK=");
	ptr2 = strstr((char*)msg.RxBuff, "&GATEWAY=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 5;
		if(ptr2-ptr1 >=7)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_netmask(tmp_buff);
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "GATEWAY=");
	ptr2 = strstr((char*)msg.RxBuff, "&SSID=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 8;
		if(ptr2-ptr1 >=7)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_gateway_ip(tmp_buff);
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "SSID=");
	ptr2 = strstr((char*)msg.RxBuff, "&WIFIPWD=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 5;
		if(ptr2-ptr1 < WIFI_SSID_BUFF_MAXLEN && ptr2-ptr1 > 0)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_wifi_conn(tmp_buff, wifi.pwd);
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "WIFIPWD=");
	ptr2 = strstr((char*)msg.RxBuff, "&WEBIP=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 8;
		if(ptr2-ptr1 < WIFI_PWD_BUFF_MAXLEN && ptr2-ptr1 > 0)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_wifi_conn(wifi.ssid,tmp_buff);
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "WEBIP=");
	ptr2 = strstr((char*)msg.RxBuff, "&WEBPORT=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 6;
		if(ptr2-ptr1 >=1)
		{
			strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
			tmp_buff[ptr2-ptr1] = '\0';
			set_webserver_ip(tmp_buff);
			taskEXIT_CRITICAL();
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "WEBLOGIN=");
	ptr2 = strstr((char*)msg.RxBuff, "&WEBPWD=");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 9;
		if(ptr2-ptr1 > 0)
		{
			if( ptr2-ptr1 < LOGIN_MAXLEN && ptr2-ptr1 > 0)
			{
				strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
				tmp_buff[ptr2-ptr1] = '\0';
				set_login(tmp_buff);
			}
		}
	}

	ptr1 = strstr((char*)msg.RxBuff, "WEBPWD=");
	ptr2 = strstr(ptr1, "\r\n");
	if(ptr1 != NULL && ptr1 != NULL)
	{
		ptr1 += 8;
		if(ptr2-ptr1 > 0)
		{
			if( ptr2-ptr1 < LOGIN_PWD_MAXLEN && ptr2-ptr1 > 0)
			{
				strncpy(&tmp_buff[0], ptr1, ptr2-ptr1);
				tmp_buff[ptr2-ptr1] = '\0';
				set_pwd(tmp_buff);
			}
		}
	}
	/*TODO: ESP Get new settings only if manually restarted.*/
}

/*
 * Task Name 	: edit_conn_params
 * Description 	: Function generates web page settings table from variables.
 */
const char * create_home_setting_str(void)
{
	static char home_page_settings[900] = {"\0"};
	sprintf(home_page_settings,
		"<p>TCP Server IP address................. %s</p>\
		<p>TCP Server port............................ %lu</p>\
		<p>Netmask........................................ %s</p>\
		<p>Gateway......................................... %s</p>\
		<p>SSID.............................................. %s</p>\
		<p>Wi-Fi password............................. %s</p>\
		<p>Webserver IP................................ %s</p>\
		<p>Webserver Port............................. %lu</p>\
		<p>login.............................................. %s</p>\
		<p>Password..................................... %s</p>", wifi.tcp_ip, wifi.tcp_port, wifi.netmask, wifi.gateway_ip, wifi.ssid, wifi.pwd, wifi.server_ip, wifi.server_port, wifi.login, wifi.login_pwd);
	return home_page_settings;
}
