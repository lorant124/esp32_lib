/*
 * debug_io.c
 *
 *  Created on: 9. 5. 2020
 *      Author: Lorant
 */
#include "debug_io.h"
#include "dma.h"
#include "usart.h"
#include "cmsis_os.h"
#include "queue.h"
#include "string.h"

extern osMessageQueueId_t debug_tx_dma_queue_idHandle;
extern osSemaphoreId_t debug_msg_sync_semHandle;

/*
 * Task Name 	: debug_uart_setup
 * Description 	: Function setups uart with DMA and idle
 * line detection for debug communication.
 */
void debug_uart_setup(void)
{
	/*UART4 RX*/
	/*LL_DMA_SetPeriphAddress(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, (uint32_t)&ESP32_UART->RDR);
	LL_DMA_SetMemoryAddress(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, (uint32_t)esp32_rx_dma_buffer);
	LL_DMA_SetDataLength(ESP32_RX_DMA, ESP32_RX_DMA_STREAM, ARRAY_LEN(esp32_rx_dma_buffer));
	//LL_DMA_EnableIT_HT(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);
	//LL_DMA_EnableIT_TC(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);
	LL_DMA_EnableStream(ESP32_RX_DMA, ESP32_RX_DMA_STREAM);

	LL_USART_EnableDMAReq_RX(ESP32_UART);
	LL_USART_EnableIT_IDLE(ESP32_UART);*/

	/*UART2 TX*/
	LL_DMA_EnableIT_TC(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM);
	LL_DMA_SetPeriphAddress(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM, (uint32_t)&DEBUG_UART->TDR);
	LL_USART_EnableDMAReq_TX(DEBUG_UART);
}

/*
 * Task Name 	: debug_print
 * Description 	: Function adds debug message to debug_tx_dma_queue_id.
 */
void debug_print(const void* data, const uint8_t len)
{
	const char* d = data;
	debug_msg_t debug_tx_tmp = {{"\0"},0};
	if(len > DEBUG_MSG_TXBUFF_LEN)
	{
		debug_print("Debug MSG is too big!\n",22);
		return;
	}
	strncpy(debug_tx_tmp.TxBuff, d, len);
	debug_tx_tmp.len = len;

	xQueueSend(debug_tx_dma_queue_idHandle, &debug_tx_tmp, 0);
}

/*
 * Task Name 	: debug_tx_dma_task
 * Description 	: Task reads debug_tx_dma_queue and sends data to tx dma function.
 */
void debug_tx_dma_task(void *argument)
{
	debug_msg_t debug_tx_tmp = {"\0",0};
	for(;;)
	{
	  osSemaphoreAcquire(debug_msg_sync_semHandle, 1000);
	  xQueueReceive(debug_tx_dma_queue_idHandle, &debug_tx_tmp, portMAX_DELAY);
	  debug_start_tx_dma_transfer(debug_tx_tmp.TxBuff,  debug_tx_tmp.len);

	  osDelay(10);
	}

}

/*
 * Task Name 	: debug_start_tx_dma_transfer
 * Description 	: Function sets up DMA to send data over UART DMA from debug_tx_dma_queue_id and enables stream.
 */
uint8_t debug_start_tx_dma_transfer(const void* data, uint8_t len)
{
	uint8_t started = 0;
    uint32_t old_primask;
    const char* d = data;
    /* Check if DMA is active */
    /* Must be set to 0 */
    old_primask = __get_PRIMASK();
    __disable_irq();

	if (len > 0) {
		/* Disable channel if enabled */
		LL_DMA_DisableStream(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM);
		/* Clear all flags */
		/*LL_DMA_ClearFlag_TC4(DEBUG_TX_DMA);
		LL_DMA_ClearFlag_HT4(DEBUG_TX_DMA);
		LL_DMA_ClearFlag_TE4(DEBUG_TX_DMA);
		LL_DMA_ClearFlag_DME4(DEBUG_TX_DMA);
		LL_DMA_ClearFlag_FE4(DEBUG_TX_DMA);*/
		/* Start DMA transfer*/
		LL_DMA_SetDataLength(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM, len);
		LL_DMA_SetMemoryAddress(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM, (uint32_t)d);

		/* Start new transfer */
		LL_DMA_EnableStream(DEBUG_TX_DMA, DEBUG_TX_DMA_STREAM);
		started = 1;
	}

    __set_PRIMASK(old_primask);
    return started;

}

