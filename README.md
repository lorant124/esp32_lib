# ESP LIB

ESP library contains 4 headers and 4 source files.
esp32.c/esp32.h defines structures, unions, helper functions and parsers.
esp32_thread.c/esp32_thread.h contains threads for esp32 handling.
debug_io.c/debug_io.h use uart for debugging. Functions in lib help to visualize messages on uart console.
Esp_lib using does not requires debug_io. 
Web_interface.c/web_interface.h contains functions for web handling such as html code.

ESP32 FW is V2.0.0, compiled 23.05.2020 from git-repo a2ca0de: https://github.com/espressif/esp-at/tree/a2ca0de7f2ca197b282925e1c293a3f0c0ea2e37  
For development Atollic TrueStudio 9.3 was used with STM32CubeMX 5.6.0.
For using this lib set up the following tasks, semaphores, mutexes and timers:
## Tasks
task name		|		entry function		|		stack size		|		priority
-------------|------------------------|--------------------|----------|
esp_coord_task|esp_coordinator_task|256|osPriorityNormal1
esp_rx_task |esp_rx_dma_task|256|osPriorityAboveNormal
esp_ctrl_cons |esp_ctrl_consumer_task |512  |osPriorityNormal7
esp_ctrl_prod |esp_ctrl_producting_task |128 |osPriorityNormal6
debug_tx_task |debug_tx_dma_task |256 |osPriorityNormal
esp_transp |esp_transparent_data_task |256 |osPriorityNormal3

## Queues
queue name|type|size
---|---|---
transparent_Queue|serv_clt_msg_t |16
esp32_rx_control_Queue|control_msg_t |16 
esp32_rx_dma_queue_id|uint8_t |10
esp32_tx_control_Queue|control_msg_t |16
debug_tx_dma_queue_id|debug_msg_t |10
usr_rx_dma_queue_id|uint8_t |16

## Mutexes
sysMutex

## Timers
|Timer Name | Callback | Type                 
|----------------|-------------------------------|-----------------------------
|ESP_MSG_Timeout01 |ESP_MSG_Timeout01_Callback |once
|ESP_MSG_Timeout02 |ESP_MSG_Timeout02_Callback |once
|ESP_MSG_Timeout03 |ESP_MSG_Timeout03_Callback |once

## Semaphores
| Semaphore Name | Callback                 
|----------------|-------------------------------
|producer_sync_sem|ESP_MSG_Timeout01_Callback 
|usr_msg_sync_sem|ESP_MSG_Timeout02_Callback 
|sys_sem|ESP_MSG_Timeout03_Callback 
|debug_msg_sync_sem|ESP_MSG_Timeout03_Callback 

- Total Heap size 25 000 - heap 4
- preemption Enable
- Generate Run Time stats Enable
- Trace facility Enable
- stats formatting function Enable
- MinimalStack size - 128

![stack monitor](https://bitbucket.org/lorant124/esp32_lib/raw/994c0169cd79bc5bc73aecd467dae7ad6476f0d7/Doc/task_stack.JPG)