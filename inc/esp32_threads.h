/*
 * esp32_threads.h
 *
 *  Created on: 12. 5. 2020
 *      Author: Lorant
 */

#ifndef ESP32_THREADS_H_
#define ESP32_THREADS_H_

extern osMessageQueueId_t esp32_rx_dma_queue_idHandle;
extern osMessageQueueId_t esp32_rx_control_QueueHandle;
extern osMessageQueueId_t esp32_tx_control_QueueHandle;
extern osMessageQueueId_t transparent_QueueHandle;
extern osSemaphoreId_t producer_sync_semHandle;
extern osSemaphoreId_t usr_msg_sync_semHandle;

extern void add_to_queue(const void* data, size_t len);
extern uint8_t AT_parser(control_msg_t msg);
extern void at_tcp_connect(uint8_t link_id);
extern void at_tcp_fixlen_str_send(uint8_t link_id, uint8_t len);
extern void debug_print(const void* data, const uint8_t lenght);
extern void esp_init(void);
extern uint8_t esp_start_tx_dma_transfer(const void* data, uint8_t len);
extern esp_alieve_enum get_esp_alieve(void);
extern uint8_t msg_str_merge(control_msg_t msg);
extern esp_state_t esp;
extern char esp32_rx_dma_buffer[RX_DMA_CIRCULAR_BUFF_LEN];

void esp_rx_dma_task(void *argument);
void esp_ctrl_consumer_task(void *argument);
void esp_ctrl_producting_task(void *argument);
void transparent_data_task(void *argument);
void esp_coordinator_task(void *argument);

#endif /* ESP32_THREADS_H_ */
