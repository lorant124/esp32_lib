/*
 * debug_io.h
 *
 *  Created on: 9. 5. 2020
 *      Author: Lorant
 */

#ifndef DEBUG_IO_H_
#define DEBUG_IO_H_

#include "stdint.h"


#define DEBUG_TX_DMA						DMA1 			/*Set Debug Uart DMA for using.*/
#define DEBUG_TX_DMA_STREAM					LL_DMA_STREAM_4	/*Set Debug Uart DMA channel for using.*/
#define DEBUG_UART							UART4			/*Set Debug Uart DMA channel for using.*/
#define DEBUG_MSG_TXBUFF_LEN				200				/*Set Debug Uart buffer lenght.*/


uint8_t debug_start_tx_dma_transfer(const void* data, uint8_t len);
void 	debug_print(const void* data, const uint8_t lenght);
void 	debug_uart_setup(void);

typedef struct {
	char TxBuff[DEBUG_MSG_TXBUFF_LEN];
	uint8_t len;
}debug_msg_t;


#endif /* DEBUG_IO_H_ */
