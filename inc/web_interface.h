/*
 * web_interface.h
 *
 *  Created on: 17. 5. 2020
 *      Author: Lorant
 */

#ifndef WEB_INTERFACE_H_
#define WEB_INTERFACE_H_

extern const char * login_page;
extern const char home_page[];
extern const char home_page_end[];
extern const char * login_error_page;
extern const char * edit_page;
const char * create_home_setting_str(void);
void client_communication(uint8_t link_id, const control_msg_t msg);
void send_html(uint8_t link_id, const char* html);
void edit_conn_params(const control_msg_t msg);
#endif /* WEB_INTERFACE_H_ */
