/*
 * esp32.h
 *
 *  Created on: 7. 5. 2020
 *      Author: Lorant
 */

#ifndef ESP32_H_
#define ESP32_H_

/****************UART INIT************************/
#define ESP32_RX_DMA	DMA1							/*Set ESP32 Uart RX DMA for using.*/
#define ESP32_RX_DMA_STREAM	LL_DMA_STREAM_5				/*Set ESP32 Uart DMA RX channel for using.*/
#define ESP32_TX_DMA	DMA1							/*Set ESP32 Uart TX DMA for using.*/
#define ESP32_TX_DMA_STREAM	LL_DMA_STREAM_6				/*Set ESP32 Uart DMA TX channel for using.*/
#define ESP32_UART	USART2								/*Set ESP32 Uart for using.*/

#define ENABLE_DEBUG_UART_TX							/*If defined lib uses debug uart from debug_io.h.*/
#define MSG_RPT_OFF										/*If defined transparent task after unsuccessfull message delivery \
														  does not repeat usr msg.*/

#define RX_DMA_CIRCULAR_BUFF_LEN		1500			/*DMA circular buffer lenght*/
#define Q_MSG_RXBUFF_LEN				200				/*RX Queue message buffer lenght*/
#define Q_MSG_TXBUFF_LEN				200				/*TX Queue message buffer lenght*/
#define WIFI_SSID_BUFF_MAXLEN			20				/*Wi-Fi SSID max lenght*/
#define WIFI_PWD_BUFF_MAXLEN			20				/*Wi-Fi password max lenght*/
#define LOGIN_MAXLEN					20				/*Web page user login max lenght*/
#define LOGIN_PWD_MAXLEN				20				/*Web page password max lenght*/

#define TCP_CLIENT_LINK_ID				0				/*TCP client link id for CIPSTART*/
#define WEB_SERVER_LINK_ID				1				/*Web server link id*/
#define IPD						"+IPD,"
#define AT						"AT+"
#define AT_OK					"OK\r\n"
#define AT_ERROR				"ERROR\r\n"
#define RDY						"\r\nready\r\n"
#define AT_CWJAP				"AT+CWJAP"
#define AT_CIPSTART				"AT+CIPSTART"
#define AT_CIPMODE				"AT+CIPMODE"
#define AT_CWMODE				"AT+CWMODE"
#define AT_CIPMUX				"AT+CIPMUX"
#define AT_CIPSTATUS			"AT+CIPSTATUS"
#define AT_CIPSEND				"AT+CIPSEND"
#define AT_CIPSERVER			"AT+CIPSERVER"
#define AT_CIPSTO				"AT+CIPSTO"
#define AT_CIPSTA				"AT+CIPSTA"
#define AT_CIPCLOSE				"AT+CIPCLOSE"
#define AT_RST					"AT+RST"
#define AT_CIPSERVERMAXCONN 	"AT+CIPSERVERMAXCONN"
#define AT_TCP_CLOSED			",CLOSED\r\n"
#define AT_TCP_CONNECT			",CONNECT\r\n"
#define RECV_SEND_OK			"SEND OK\r\n"
#define RECV_SEND_ERROR			"SEND FAIL\r\n"

#define ARRAY_LEN(x)       (sizeof(x) / sizeof((x)[0]))	/*MACRO for array lenght calculation with linker*/

/*data type for transparent Queue*/
typedef struct{
	char message[Q_MSG_TXBUFF_LEN];
	uint8_t len;
	uint8_t link_id;
}serv_clt_msg_t;

/*data type for Control Queue*/
typedef struct{
	char RxBuff[Q_MSG_RXBUFF_LEN];
	uint8_t len;
}control_msg_t;

typedef enum{
	NOTALIEVE = 0,
	ALIEVE,
	RESTART
}esp_alieve_enum;

typedef enum{
	DISCONNECTED = 0,
	CONNECTED,
	CONNECTION_ERROR,
	CONNECTED_W_IP,
}esp_conn_enum;

typedef enum{
	CLOSED = 0,
	TCP_CONNECTED,
	TCP_ERROR
}esp_tcp_conn_enum;

typedef enum{
	MSG_UNKNOWN = 0,
	MSG_AT,
	MSG_RDY,
	MSG_CLOSE,
	MSG_SEND,
	MSG_IPD,
	MSG_CONNECT,
	MSG_CLIENT_DISCONNECT,
	MSG_CLIENT_CONNECT,
	MSG_URC
}urc_ans_enum;

typedef struct {
	esp_alieve_enum alieve; 		/*esp alieve state*/
	esp_conn_enum conn;				/*esp connection state*/
	esp_tcp_conn_enum tcp_conn;		 /*esp tcp connection state*/
	uint8_t conn_err_code;
	uint8_t tcp_conn_err_code;
}esp_state_t;

typedef struct{
	char ssid[WIFI_SSID_BUFF_MAXLEN];
	char pwd[WIFI_PWD_BUFF_MAXLEN];
	char tcp_ip[16];
	char login[LOGIN_MAXLEN];
	char login_pwd[LOGIN_PWD_MAXLEN];
	uint32_t tcp_port;
	char server_ip[16];
	uint32_t server_port;
	char gateway_ip[16];
	char netmask[16];
}wifi_ap_t;

extern osMessageQueueId_t esp32_rx_dma_queue_idHandle;
extern osMessageQueueId_t esp32_rx_control_QueueHandle;
extern osMessageQueueId_t esp32_tx_control_QueueHandle;
extern osMessageQueueId_t transparent_QueueHandle;

extern osSemaphoreId_t producer_sync_semHandle;
extern osSemaphoreId_t usr_msg_sync_semHandle;

extern osTimerId_t ESP_MSG_Timeout01Handle;
extern osTimerId_t ESP_MSG_Timeout02Handle;
extern osTimerId_t ESP_MSG_Timeout03Handle;

urc_ans_enum 	parse_urc(const control_msg_t msg);
esp_conn_enum 	get_esp_conn(void);
esp_alieve_enum get_esp_alieve(void);
urc_ans_enum 	msg_str_merge(control_msg_t msg);

uint8_t esp_start_tx_dma_transfer(const void* data, uint8_t len);
uint8_t AT_parser(control_msg_t msg);
uint8_t parse_cwjap(const control_msg_t msg);
uint8_t parse_cipstart(const control_msg_t msg);
uint8_t parse_cipmode(const control_msg_t msg);
uint8_t parse_cipsend(const control_msg_t msg);
uint8_t parse_cipstatus(const control_msg_t msg);
uint8_t parse_cwmode(const control_msg_t msg);
uint8_t parse_cipservermaxconn(const control_msg_t msg);
uint8_t parse_cipmux(const control_msg_t msg);
uint8_t parse_cipserver(const control_msg_t msg);
uint8_t parse_cipsto(const control_msg_t msg);
uint8_t parse_cipsta(const control_msg_t msg);
uint8_t parse_rst(const control_msg_t msg);
uint8_t parse_cipclose(const control_msg_t msg);

void set_esp_tcp_conn(esp_tcp_conn_enum tmp, uint8_t err_code);
void set_esp_alieve(esp_alieve_enum tmp);
void esp_sw_rst(void);
void esp_init(void);
void send_AT(const void* data, uint8_t len);
void set_wifi_conn(const void* ssid, const void* pwd);
void set_wifi_tcp_conn(const void* ip, const uint32_t port);
void at_wifi_connect(void);
void at_tcp_connect(uint8_t link_id);
void set_webserver_port(uint32_t port);
void at_webserver_start(void);
void at_tcp_fixlen_str_send(uint8_t link_id, uint8_t len);
void DMA1_Stream5_IRQHandlerX(void);
void set_webserver_ip(const void* ip);
void set_gateway_ip(const void* ip);
void set_netmask(const void* netmask);
void set_login(const void* login);
void set_pwd(const void* pwd);
void esp32_UART_Handler(void);
void esp32_uart_setup(void);
void add_to_queue(const void* data, size_t len);
void set_esp_conn(esp_conn_enum tmp, uint8_t err_code);
#endif /* ESP32_H_ */
